<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>RPG</title>
</head>
<body>

    <?php
        include('./entity/LifeEntity.php');
        include('./entity/hero.php');
        $melchior = new Hero('Melchior'); // calling magic method __constuct
        $melchior->talk(); // calling public method talk
        $melchior->set_sleep(25);
        $melchior->set_life(97);

        $erwann = new Hero('Erwann');
        $erwann->talk();
        $erwann->set_sleep(50);
        $erwann->set_life(2);
        
        echo "<br>";

        $melchior->talk();
        $erwann->talk();
        
        echo "<br>";
 
        include('./entity/monster.php');
        $loup_affame = new Monster('Loup affamé');
        $loup_affame->talk();

        echo "<br>";
        
        $loup_affame->attack($erwann);

        echo "<br>";

        $melchior->attack($erwann);
        
        echo "<br>";

        $erwann->talk();
        $melchior->talk();
    ?>
</body>
</html>