<?php
    abstract class LifeEntity {
        protected $life;
        protected $name;
        protected $damage;

        public function __construct($name, $life, $damage) {
            $this->name = $name;
            $this->life = $life;
            $this->damage = $damage;
        }

        abstract function talk();

        public function get_life():int {
            return $this->life;
        }
        public function get_name():string {
            return $this->name;
        }
        public function set_life(int $life) {
            $this->life = $life;
        }
        public function get_damage():int {
            return $this->damage;
        }

        public function attack(LifeEntity $damaged) {
            echo "$this->name attaque ".$damaged->get_name()."<br>";
            $damaged->has_Damage($this);
        }

        public function has_Damage(LifeEntity $damager){
            $this->set_life($this->get_life() - $damager->get_damage());
            
            if($this->isDeath()){
                echo "$this->name a été tué par ".$damager->name." <br>";
                $this->set_life(0);
            }
        }
        
        public function isDeath():bool {
            return $this->life < 0;
        }
    }
?>