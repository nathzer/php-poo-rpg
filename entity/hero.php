<?php

class Hero extends LifeEntity {
    private $sleep = 0;

    public function __construct($name) {
        parent::__construct($name, 100, 50);
        $this->set_sleep(0);
    }
    public function talk() {
        echo "Hi, I am $this->name, I have $this->life point(s) of life and $this->sleep point(s) of sleep.<br>"; 
    } 
    
    public function is_tired():bool {
        return ($this->sleep >= 100);
    }
    public function send_tiredMessage(LifeEntity $damaged){
        echo "$this->name est trop fatigué. Il ne peux pas attaquer ".$damaged->get_name()." <br>";
    }
    public function set_sleep(int $sleep) {
        $this->sleep = $sleep;

        if($this->is_tired())
            $this->sleep = 100;
    }

    public function attack(LifeEntity $damaged){
        if($this->is_tired()){
            $this->send_tiredMessage($damaged);
            return;
        }
        $this->set_sleep($this->sleep + 15);
        parent::attack($damaged);
    }
    public function has_Damage(LifeEntity $damager){
        if(random_int(0, 100) < 80){ //80% de chances
            if(!$this->is_tired()){
                echo "$this->name vient de contrer l'attaque de ".$damager->get_name()." <br>";
                $this->attack($damager);
                return;
            }
            $this->send_tiredMessage($damager);
        }
        parent::has_Damage($damager);
    }

}